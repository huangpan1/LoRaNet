package com.hackathon.lola;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;

import com.hackathon.lola.fragment.AboutUsFragment;
import com.hackathon.lola.fragment.HistoryFragment;
import com.hackathon.lola.fragment.HomeFragment;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private ResideMenu reside_menu_admin;
    private MainActivity mContext;
    private ResideMenuItem itemDashboard;
    private ResideMenuItem itemAboutUs;
    private ResideMenuItem itemHistory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        setUpMenu();
        if (savedInstanceState == null) {
            changeFragment(new HomeFragment());
        }

        // This snippet shows the system bars. It does this by removing all the flags
        // except for the ones that make the content appear under the system bars.
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

    }

    private void setUpMenu() {
        // attach to current activity;
        reside_menu_admin = new ResideMenu(this);
        reside_menu_admin.setBackground(R.drawable.swap_background);
        reside_menu_admin.attachToActivity(this);
        reside_menu_admin.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        reside_menu_admin.setScaleValue(0.6f);
        reside_menu_admin.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        /** above code for setting in menu **/

        // create menu items;
        itemDashboard = new ResideMenuItem(this, R.drawable.icon_home, "Monitor");
        itemHistory = new ResideMenuItem(this, R.drawable.icon_calendar, "History");
        itemAboutUs = new ResideMenuItem(this, R.drawable.icon_profile, "About Us");

        itemDashboard.setOnClickListener(this);
        itemAboutUs.setOnClickListener(this);
        itemHistory.setOnClickListener(this);

        reside_menu_admin.addMenuItem(itemDashboard, ResideMenu.DIRECTION_LEFT);
        reside_menu_admin.addMenuItem(itemHistory, ResideMenu.DIRECTION_LEFT);
        reside_menu_admin.addMenuItem(itemAboutUs, ResideMenu.DIRECTION_LEFT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reside_menu_admin.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return reside_menu_admin.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemDashboard) {
            changeFragment(new HomeFragment());
        } else if (view == itemAboutUs) {
            changeFragment(new AboutUsFragment());
        } else if( view == itemHistory){
            changeFragment(new HistoryFragment());
        }

        reside_menu_admin.closeMenu();
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
        }

        @Override
        public void closeMenu() {
        }
    };

    private void changeFragment(Fragment targetFragment) {
        reside_menu_admin.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access reside_menu_admin？
    public ResideMenu getReside_menu_admin() {
        return reside_menu_admin;
    }

}
