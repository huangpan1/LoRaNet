package com.hackathon.lola.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hackathon.lola.R;
import com.hackathon.lola.model.GetLatestModel;
import com.hackathon.lola.view.StatusTextView;

import java.io.IOException;
import java.util.List;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by ernestwong on 3/6/2017.
 */

public class PreviewAdapter extends BaseAdapter {

    private List<GetLatestModel.LatestData> dataSet;
    private Context context;
    private int backgroundId;

    public PreviewAdapter(List<GetLatestModel.LatestData> dataSet , Context context , int backgroundId) {
        this.dataSet = dataSet;
        this.context = context;
        this.backgroundId = backgroundId;
    }

    @Override
    public int getCount() {
        return dataSet.size() + 1;
    }

    @Override
    public GetLatestModel.LatestData getItem(int i) {
        return dataSet.get(i - 1);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder{
        StatusTextView nodeId;
        StatusTextView nodeText;
        GifImageView imageView;
        StatusTextView node_value_1, node_value_2 , node_value_3;
        StatusTextView timeStamp;
        StatusTextView status;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder; // view lookup cache stored in tag

//        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.listview_preview, viewGroup, false);
            viewHolder.nodeId = (StatusTextView) convertView.findViewById(R.id.data_id);
            viewHolder.nodeText = (StatusTextView) convertView.findViewById(R.id.node_name);
            viewHolder.imageView = (GifImageView) convertView.findViewById(R.id.gif);
            viewHolder.timeStamp = (StatusTextView) convertView.findViewById(R.id.time_stamp);
            viewHolder.status =  (StatusTextView) convertView.findViewById(R.id.status);
            viewHolder.node_value_1 = (StatusTextView) convertView.findViewById(R.id.data_value_1);
            viewHolder.node_value_2 = (StatusTextView) convertView.findViewById(R.id.data_value_2);
            viewHolder.node_value_3 = (StatusTextView) convertView.findViewById(R.id.data_value_3);


//            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
//            result=convertView;
        }

        convertView.setBackgroundResource(backgroundId);

        if(i == 0){ //header
            viewHolder.imageView.setVisibility(View.GONE);
            viewHolder.nodeId.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.nodeId.setText("Node Id");
            viewHolder.nodeText.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.nodeText.setText("Node Name");
            viewHolder.status.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.status.setText("Status");
            viewHolder.node_value_1.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.node_value_1.setText("Value 1");
            viewHolder.node_value_2.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.node_value_2.setText("Value 2");
            viewHolder.node_value_3.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.node_value_3.setText("Value 3");
        }else {
            GetLatestModel.LatestData model = getItem(i);
            viewHolder.timeStamp.setTypeface(Typeface.DEFAULT);
            viewHolder.nodeId.setTypeface(Typeface.DEFAULT);
            viewHolder.nodeText.setTypeface(Typeface.DEFAULT);
            viewHolder.node_value_1.setTypeface(Typeface.DEFAULT);
            viewHolder.node_value_2.setTypeface(Typeface.DEFAULT);
            viewHolder.node_value_3.setTypeface(Typeface.DEFAULT);
            viewHolder.status.setTypeface(Typeface.DEFAULT_BOLD);

            viewHolder.timeStamp.setText(model.getTimestamp());
            viewHolder.nodeId.setText(model.getNode_id());
            viewHolder.nodeText.setText(model.getNode_name());
//            viewHolder.node_value_1.setTextColor(ActivityCompat.getColor(context , R.color.color_text_value_1));
//            viewHolder.node_value_2.setTextColor(ActivityCompat.getColor(context , R.color.color_text_value_2));
            viewHolder.node_value_1.setText(model.getData().getValue1());
            viewHolder.node_value_2.setText(model.getData().getValue2());
            viewHolder.node_value_3.setText(model.getData().getValue3());

            //status -> default disconnect
            viewHolder.status.setStatus(model.getStatus());

            GifDrawable drawable = null;
            try {
                drawable = new GifDrawable(context.getResources() , R.drawable.wifi_2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            drawable.setLoopCount(0);
            viewHolder.imageView.setImageDrawable(drawable);
        }



        return convertView;
    }
}
