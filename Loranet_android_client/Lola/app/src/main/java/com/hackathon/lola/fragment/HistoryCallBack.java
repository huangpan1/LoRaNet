package com.hackathon.lola.fragment;

import com.hackathon.lola.model.GetAnalyzeDataModel;

/**
 * Created by ernestwong on 4/6/2017.
 */

public interface HistoryCallBack{
    void onSuccess(GetAnalyzeDataModel model , String data_id);
}