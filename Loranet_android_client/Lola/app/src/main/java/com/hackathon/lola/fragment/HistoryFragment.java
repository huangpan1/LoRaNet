package com.hackathon.lola.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.hackathon.lola.R;
import com.hackathon.lola.model.GetAnalyzeDataModel;
import com.hackathon.lola.network.CallBackInterface;
import com.hackathon.lola.network.NetworkRequest;
import com.hackathon.lola.view.ChartBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ernestwong on 4/6/2017.
 */

public class HistoryFragment extends Fragment implements HistoryCallBack{

    private View parentView;

    TreeMap<String , LineDataSet> dataList;
    TreeMap<String, LineDataSet> node1Map, node2Map, node3Map;
    private LineChart mChart;
    private Spinner spinner1, spinner2;

    private boolean isFirstTime = true;

    private static final String value11_1 = "Node A - 1";
    private static final String value11_2 = "Node A - 2";
    private static final String value11_3 = "Node A - 3";

    private static final String value12_1 = "Node B - 1";
    private static final String value12_2 = "Node B - 2";
    private static final String value12_3 = "Node B - 3";

    private static final String value13_1 = "Node C - 1";
    private static final String value13_2 = "Node C - 2";
    private static final String value13_3 = "Node C - 3";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_history, container, false);
        ((TextView)getActivity().findViewById(R.id.title)).setText("LORA History");
        return parentView;
    }

    @Override
    public void onSuccess(GetAnalyzeDataModel model, String data_id) {
        if(data_id .equals("11")){
            generateToDataList(model.getData().getValue1() , value11_1 , Color.YELLOW , 0);
            generateToDataList(model.getData().getValue2() , value11_2 , Color.RED , 0);
            generateToDataList(model.getData().getValue3() , value11_3 , Color.WHITE , 0);
            getDetailsById("12" , this);
        }

        if(data_id.equals("12")){
            generateToDataList(model.getData().getValue1() , value12_1 , Color.GRAY , 1);
            generateToDataList(model.getData().getValue2() , value12_2 , Color.LTGRAY , 1);
            generateToDataList(model.getData().getValue3() , value12_3 , Color.GREEN , 1);
            getDetailsById("13" , this);
        }

        if(data_id.equals("13")){
            generateToDataList(model.getData().getValue1() , value13_1 , Color.MAGENTA , 2);
            generateToDataList(model.getData().getValue2() , value13_2 , Color.CYAN , 2);
            generateToDataList(model.getData().getValue3() , value13_3 , Color.BLUE , 2);
            showSpinner2();

        }

    }



    private void getDetail(){
        getDetailsById("11", this);
    }

    private void getDetailsById(final String data_id , final HistoryCallBack callBack){
        NetworkRequest request = new NetworkRequest("get_data_details.php?node_id=" + data_id){

            @Override
            public CallBackInterface getCallBack() {
                return new CallBackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {
                        Gson gson = new Gson();
                        GetAnalyzeDataModel model = gson.fromJson(response , GetAnalyzeDataModel.class);
                        callBack.onSuccess(model , data_id);
                    }
                };
            }
        };

        request.sendAPI();
    }

//    private void getDetails(){
//        NetworkRequest request = new NetworkRequest("get_data_details.php?node_id=11") {
//            @Override
//            public CallBackInterface getCallBack() {
//                return new CallBackInterface() {
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, String response) {
//                        Gson gson = new Gson();
//                        GetAnalyzeDataModel model = gson.fromJson(response , GetAnalyzeDataModel.class);
//                        generateToDataList(model.getData().getValue1() , value1_1 , Color.YELLOW , 1);
//                        generateToDataList(model.getData().getValue2() , value1_2 , Color.BLACK , 1);
//                        generateToDataList(model.getData().getValue3() , value1_3 , Color.WHITE , 1);
//
//                        NetworkRequest request2 = new NetworkRequest("get_data_details.php?node_id=2") {
//                            @Override
//                            public CallBackInterface getCallBack() {
//                                return  new CallBackInterface() {
//                                    @Override
//                                    public void onSuccess(int statusCode, Header[] headers, String response) {
//                                        Gson gson = new Gson();
//                                        GetAnalyzeDataModel model = gson.fromJson(response , GetAnalyzeDataModel.class);
//                                        generateToDataList(model.getData().getValue1() , value2_1 , Color.GREEN , 0);
//                                        generateToDataList(model.getData().getValue2() , value2_2 , Color.BLUE , 0);
//                                        generateToDataList(model.getData().getValue3() , value2_3 , Color.RED , 0);
//
//                                        showSpinner2();
//
//                                    }
//                                };
//                            }
//                        };
//
//                        request2.sendAPI();
//
//                    }
//                };
//            }
//        };
//
//        request.sendAPI();
//
//    }

    private int selected_first = 0;

    private void showSpinner2(){
        spinner2 = (Spinner) parentView.findViewById(R.id.spinner_second_level);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount(); // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add("All Values");
        if(dataList != null) {
            for (String key : dataList.keySet()) {
                adapter.add(key);
            }
        }

        spinner2.setAdapter(adapter);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TreeMap<String , LineDataSet> targetMap = new TreeMap<>();

                switch (i){
                    case 0:
                        targetMap = dataList;
                        break;
                    default:
                        targetMap.put(new ArrayList<>(dataList.keySet()).get(i - 1) , new ArrayList<>(dataList.values()).get(i - 1));
                        break;
                }

                mChart = new ChartBuilder(parentView , R.id.lineChart)
                        .setData(new ArrayList<>(targetMap.values()))
                        .setAnimated(true , 0)
                        .setYAxisLimit(maxValue * 1.2f, minValue * 0.8f)
                        .build();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner1 = (Spinner) view.findViewById(R.id.spinner_first_level);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item
        ) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount(); // you dont display last item. It is used as hint.
            }

        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter.add("All Nodes");
        adapter.add("Node A");
        adapter.add("Node B");
        adapter.add("Node C");

        spinner1.setAdapter(adapter);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dataList.clear();
                switch (i){
                    case 0:
                        dataList.putAll(node1Map);
                        dataList.putAll(node2Map);
                        dataList.putAll(node3Map);
                        break;
                    case 1:
                        dataList.putAll(node1Map);
                        break;
                    case 2:
                        dataList.putAll(node2Map);
                        break;
                    case 3:
                        dataList.putAll(node3Map);
                }
                showSpinner2();
                spinner2.setSelection(0);

                genChart();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        showSpinner2();

//        spinner.setSelection(adapter_1.getCount()); //display hint

        dataList = new TreeMap<>();
        node1Map = new TreeMap<>();
        node2Map = new TreeMap<>();
        node3Map = new TreeMap<>();
//        getDetails();

        getDetail();

    }

    private void genChart(){
        mChart = new ChartBuilder(parentView , R.id.lineChart)
                .setData(new ArrayList<>(dataList.values()))
                .setAnimated(true , 0)
                .setYAxisLimit(maxValue * 1.2f , minValue * 0.8f)
                .build();
    }

    private float maxValue =0f , minValue = 0f;

    private void generateToDataList(List<Float> arrayModel , String label , int color , int y){
        ArrayList<Entry> floatList = new ArrayList<>();
        Collections.reverse(arrayModel);
        for(int i = 0; i < arrayModel.size() ; i++){
            floatList.add( new Entry(i , arrayModel.get(i)));
            if(arrayModel.get(i) > maxValue){
                maxValue = arrayModel.get(i);
            }else if(arrayModel.get(i) < minValue){
                minValue = arrayModel.get(i);
            }
        }

        LineDataSet sets = initDataSet(floatList , label);
        sets= analysis(sets , color);

        dataList.put(label , sets);

        if( y == 0){
            node1Map.put(label , sets);
        }

        if( y == 1){
            node2Map.put(label , sets);
        }

        if(y == 2){
            node3Map.put(label , sets);
        }
    }

    private LineDataSet initDataSet( ArrayList<Entry> dataSet , String label){
        LineDataSet set = new LineDataSet(dataSet, label);
        return set;
    }

    private LineDataSet analysis(LineDataSet set , int lineColor){
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setCubicIntensity(0.2f);
        set.setDrawFilled(true);
        set.setDrawCircles(false);
        set.setLineWidth(2.5f);
        set.setCircleRadius(4f);
        set.setCircleColor(Color.YELLOW);
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setHighlightLineWidth(2f);
        set.setColor(lineColor);
        set.setFillColor(lineColor);
        set.setFillAlpha(60);
        set.setDrawHorizontalHighlightIndicator(false);
        set.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return -10;
            }
        });

        return set;
    }
}
