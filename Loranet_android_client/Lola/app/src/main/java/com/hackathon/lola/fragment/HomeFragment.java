package com.hackathon.lola.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.hackathon.lola.MainActivity;
import com.hackathon.lola.R;
import com.hackathon.lola.adapter.PreviewAdapter;
import com.hackathon.lola.model.GetAnalyzeDataModel;
import com.hackathon.lola.model.GetLatestModel;
import com.hackathon.lola.network.CallBackInterface;
import com.hackathon.lola.network.NetworkRequest;
import com.hackathon.lola.view.ChartView;
import com.special.ResideMenu.ResideMenu;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ernestwong on 3/6/2017.
 */
public class HomeFragment extends Fragment {

    private View parentView;
    private ResideMenu resideMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_home_v2, container, false);
        ((TextView)getActivity().findViewById(R.id.title)).setText("LORA Dashboard");
        setUpViews();
        return parentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpListView(view);
    }

    private void setUpViews() {
        MainActivity parentActivity = (MainActivity) getActivity();
        resideMenu = parentActivity.getReside_menu_admin();
    }

    PreviewAdapter adapter_1, adapter_2 , adapter_3;
    ListView listView_1, listView_2 , listView_3;
    List<GetLatestModel.LatestData> dataList = new ArrayList<>();

    private void setUpListView(View view){
        listView_1 =(ListView) view.findViewById(R.id.list_view);
        adapter_1 = new PreviewAdapter(dataList , getActivity() , R.drawable.gradient_background);
        listView_1.setAdapter(adapter_1);
        listView_1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
                    getAnalysisData(dataList.get(i - 1).getNode_id() , dataList.get(i - 1).getNode_name());
                }
            }
        });

        listView_2 = (ListView) view.findViewById(R.id.list_view_2);
        adapter_2 = new PreviewAdapter(dataList , getActivity() , R.drawable.gradient_background_item_2);
        listView_2.setAdapter(adapter_2);
        listView_2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
                    getAnalysisData(dataList.get(i - 1).getNode_id() , dataList.get(i - 1).getNode_name());
                }
            }
        });

        listView_3 = (ListView) view.findViewById(R.id.list_view_3);
        adapter_3 = new PreviewAdapter(dataList , getActivity() , R.drawable.gradient_background_item_3);
        listView_3.setAdapter(adapter_3);
        listView_3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0) {
                    getAnalysisData(dataList.get(i - 1).getNode_id() , dataList.get(i - 1).getNode_name());
                }
            }
        });

    }

    private boolean started = false;
    private Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            testHttpRequest();
            if(started) {
                start();
            }
        }
    };

    public void stop() {
        started = false;
        handler.removeCallbacks(runnable);
    }

    public void start() {
        started = true;
        handler.postDelayed(runnable, 5000);
    }

    @Override
    public void onResume() {
        super.onResume();
        testHttpRequest();
        start();
    }

    @Override
    public void onPause() {
        super.onPause();
        stop();
    }

    private void getAnalysisData(String nodeId, final String nodeName){
        NetworkRequest request = new NetworkRequest("get_data_details.php?node_id=" + nodeId) {

            @Override
            public CallBackInterface getCallBack() {
                return new CallBackInterface() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {
                        Gson gson = new Gson();
                        Log.d("home fragment", "success2 " + statusCode + " " + new String(response));
                        GetAnalyzeDataModel model = gson.fromJson(new String(response) , GetAnalyzeDataModel.class);
                        showAnalytics(getActivity() , model.getData(), nodeName);

                    }
                };
            }
        };
        request.sendAPI();
    }

    private void showAnalytics(Activity context, GetAnalyzeDataModel.DetailModel model, String nodeName){

        ChartView chartView = new ChartView(context, model , nodeName);

        AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity())
                .setView(chartView)
                .create();

        if (dialogBuilder.getWindow() != null) {
//            dialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            WindowManager.LayoutParams params = dialogBuilder.getWindow().getAttributes(); // change this to your dialog.
//            params.gravity = Gravity.TOP | Gravity.END | Gravity.RIGHT ;
//            params.y = yPos; // Here is the param to set your dialog position. Same with params.x
//            dialogBuilder.getWindow().setAttributes(params);
        }

        dialogBuilder.setCancelable(true);
        dialogBuilder.show();
    }

    private void testHttpRequest(){
        NetworkRequest request = new NetworkRequest("get_data.php") {

            @Override
            public CallBackInterface getCallBack() {
                return new CallBackInterface() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {
                        Gson gson = new Gson();
                        GetLatestModel model = gson.fromJson(new String(response) , GetLatestModel.class);
                        dataList = model.getData();
                        adapter_1 = new PreviewAdapter(model.getData() , getActivity() , R.drawable.gradient_background);
                        listView_1.setAdapter(adapter_1);

                        adapter_2 = new PreviewAdapter(model.getData() , getActivity() , R.drawable.gradient_background_item_2);
                        listView_2.setAdapter(adapter_2);

                        adapter_3 = new PreviewAdapter(model.getData() , getActivity() , R.drawable.gradient_background_item_3);
                        listView_3.setAdapter(adapter_3);
                    }
                };
            }
        };
        request.sendAPI();
    }



}