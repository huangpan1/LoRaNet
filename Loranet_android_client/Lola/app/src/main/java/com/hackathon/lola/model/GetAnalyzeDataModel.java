package com.hackathon.lola.model;

import java.util.List;

/**
 * Created by ernestwong on 3/6/2017.
 */

public class GetAnalyzeDataModel {

    private String status;

    private DetailModel data;

    public String getStatus() {
        return status;
    }

    public DetailModel getData() {
        return data;
    }

    public static class DetailModel{
        List<String> timestamp;
        List<Float> value1;
        List<Float> value2;
        List<Float> value3;

        public List<String> getTimestamp() {
            return timestamp;
        }

        public List<Float> getValue1() {
            return value1;
        }

        public List<Float> getValue2() {
            return value2;
        }

        public List<Float> getValue3() {
            return value3;
        }
    }

}
