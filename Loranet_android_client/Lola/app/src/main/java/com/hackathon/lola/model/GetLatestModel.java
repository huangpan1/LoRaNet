package com.hackathon.lola.model;

import java.util.List;

/**
 * Created by ernestwong on 3/6/2017.
 */


/**
 *
 * {
     "status": "200",
     "data": [
     {
     "data_id": "1",
     "node_id": "1",
     "node_name": "A",
     "timestamp": "xxxxxxxx",
     "data": {
     "type": "xxxxxxx"
     }
     },
     {
     "data_id": "2",
     "node_id": "2",
     "node_name": "B",
     "timestamp": "xxxxxxxx",
     "data": {
     "type": "xxxxxxx"
     }
     }
     ]
     }
 */

public class GetLatestModel {

    private String status;

    private List<LatestData> data;

    public String getStatus() {
        return status;
    }

    public List<LatestData> getData() {
        return data;
    }

    public static class LatestData{

        private String data_id;
        private String node_id;
        private String node_name;
        private String timestamp;
        private DetailData data;
        private int status;

        public String getData_id() {
            return data_id;
        }

        public String getNode_id() {
            return node_id;
        }

        public String getNode_name() {
            return node_name;
        }

        public DetailData getData() {
            return data;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public int getStatus() {
            return status;
        }
    }

    public static class DetailData{
        private String value1;
        private String value2;
        private String value3;

        public String getValue1() {
            return value1;
        }

        public String getValue2() {
            return value2;
        }

        public String getValue3() {
            return value3;
        }
    }

}
