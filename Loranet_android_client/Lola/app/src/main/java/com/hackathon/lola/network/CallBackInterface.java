package com.hackathon.lola.network;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ernestwong on 3/6/2017.
 */

public interface CallBackInterface {
    void onSuccess(int statusCode, Header[] headers, String response);
}
