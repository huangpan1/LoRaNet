package com.hackathon.lola.network;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ernestwong on 3/6/2017.
 */

public abstract class NetworkRequest {
    private static final String DOMAIN = "http://34.211.132.145/";

    private String path;

    public abstract CallBackInterface getCallBack();

    public NetworkRequest(String path ){
        this.path = path;
    }

    public void sendAPI(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(DOMAIN + path, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d("network", "success2 " + statusCode + " " + new String(responseBody));
                getCallBack().onSuccess(statusCode , headers , new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                Snackbar.make(this , "status : " + statusCode + " " + new String(responseBody) , Snackbar.LENGTH_LONG);
            }
        });
    }


}
