package com.hackathon.lola.view;

import android.graphics.Color;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ernestwong on 3/6/2017.
 */

public class ChartBuilder {

    private LineChart mChart;
    private LineData data;

    private YAxis yLeftAxis;
    private YAxis yRightAxis;
    private XAxis xAxis;

    public ChartBuilder(View view , int resId) {
        mChart = (LineChart) view.findViewById(resId);
        data = new LineData();
        yLeftAxis = mChart.getAxisLeft();
        yRightAxis = mChart.getAxisRight();
        xAxis = mChart.getXAxis();
        basicConfig();
    }

    private ChartBuilder basicConfig(){
        mChart.getDescription().setEnabled(false);

        mChart.setDrawGridBackground(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

//        yLeftAxis.setAxisMaximum(5f);
//        yLeftAxis.setAxisMinimum(0f);
        yRightAxis.setEnabled(false);
        yLeftAxis.setAxisLineColor(Color.WHITE);
        yLeftAxis.setTextColor(Color.WHITE);

        xAxis.setEnabled(false);

        return this;
    }

    public ChartBuilder setYAxisLimit(float max , float min){
        yLeftAxis.setAxisMaximum(max);
        yLeftAxis.setAxisMinimum(min);
        yLeftAxis.setMinWidth(30f);
        return this;
    }

    public ChartBuilder setAnimated(boolean isAnimated , int animationTime){
        if(isAnimated) {
            mChart.animateX(animationTime);
        }else{
            mChart.animateX(0);
        }

        return this;
    }

    public ChartBuilder setTextSize(float textSize){
        data.setValueTextSize(9f);
        data.setDrawValues(true);
        return this;
    }

    public ChartBuilder setData(List<LineDataSet> dataSets){
        List<ILineDataSet> set = new ArrayList<>();
        for( LineDataSet temp_set : dataSets){
            set.add(temp_set);
        }

        data = new LineData(set);
        data.setDrawValues(false);
        mChart.setData(data);
        return this;
    }

    public LineChart build(){
        mChart.invalidate();

        return mChart;
    }

}
