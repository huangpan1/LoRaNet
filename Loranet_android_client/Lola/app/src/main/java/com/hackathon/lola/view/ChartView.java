package com.hackathon.lola.view;

import android.app.Activity;
import android.graphics.Color;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.hackathon.lola.R;
import com.hackathon.lola.model.GetAnalyzeDataModel;
import com.hackathon.lola.model.GetLatestModel;
import com.hackathon.lola.network.CallBackInterface;
import com.hackathon.lola.network.NetworkRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by ernestwong on 3/6/2017.
 */

public class ChartView extends FrameLayout{
    private LineChart mChart;
    List<LineDataSet> dataList;

    Activity activity;

    private float maxValue = 0f, minValue = 0f;

    private void generateToDataList(List<Float> arrayModel , String label , int color){
        ArrayList<Entry> floatList = new ArrayList<>();
        //TODO: reverse order
        Collections.reverse(arrayModel);
        for(int i = 0; i < arrayModel.size() ; i++){
            floatList.add( new Entry(i , arrayModel.get(i)));
            if(arrayModel.get(i) > maxValue){
                maxValue = arrayModel.get(i);
            }else if(arrayModel.get(i) < minValue){
                minValue = arrayModel.get(i);
            }
        }

        LineDataSet sets = initDataSet(floatList , label);
        sets= analysis(sets , color);

        dataList.add(sets);
    }

    private void addEntry() {

        final LineData data = mChart.getData();

        if (data != null) {

            final ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

//            if (set == null) {
//                set = createSet();
//                data.addDataSet(set);
//            }

//            data.addEntry(new Entry(set.getEntryCount(), (float) (Math.random() * 6) + 0f), 0);

            NetworkRequest request = new NetworkRequest("get_data.php") {

                @Override
                public CallBackInterface getCallBack() {
                    return new CallBackInterface() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String response) {
                            Gson gson = new Gson();
                            GetLatestModel model = gson.fromJson(new String(response) , GetLatestModel.class);
                            for(GetLatestModel.LatestData datas : model.getData()){
                                if(datas.getNode_name().equals(nodeName)){
                                    if( dataList.size() >= 3){
                                        data.addEntry(new Entry(data.getDataSetByIndex(0).getEntryCount() ,  Float.parseFloat(datas.getData().getValue1())) , 0);
                                        data.addEntry(new Entry(data.getDataSetByIndex(1).getEntryCount() ,  Float.parseFloat(datas.getData().getValue2())) , 1);
                                        data.addEntry(new Entry(data.getDataSetByIndex(2).getEntryCount() ,  Float.parseFloat(datas.getData().getValue3())) , 2);
                                    }
                                }
                            }
                        }
                    };
                }
            };
            request.sendAPI();

            data.notifyDataChanged();

            // let the chart know it's data has changed
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(120);
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            mChart.moveViewToX(data.getEntryCount());

            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private Thread thread;

    private void feedMultiple() {

        if (thread != null)
            thread.interrupt();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                addEntry();
            }
        };

        thread = new Thread(new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    // Don't generate garbage runnables inside the loop.
                    activity.runOnUiThread(runnable);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }

    String nodeName;

    public ChartView(Activity context , GetAnalyzeDataModel.DetailModel model , String nodeName) {
        super(context);
        this.activity = context;
        dataList = new ArrayList<>();

        generateToDataList(model.getValue1() , "Value 1" , Color.GREEN);
        generateToDataList(model.getValue2() , "Value 2" , Color.BLUE);
        generateToDataList(model.getValue3() , "Value 3" , Color.RED);

        this.nodeName = nodeName;

        init();
    }

    private void init(){
        inflate(getContext() , R.layout.fragment_chart , this);
        TextView titleView = (TextView) findViewById(R.id.chart_title);
        titleView.setText(String.format("Real Time Charts - Node %1$s" , nodeName));
        initChart3();
        feedMultiple();
    }

    private void initChart3(){
        mChart = new ChartBuilder(this , R.id.lineChart)
                .setData(dataList)
                .setAnimated(true , 2000)
                .setYAxisLimit(maxValue * 1.2f , minValue * 0.8f)
                .build();

    }

    private LineDataSet initDataSet( ArrayList<Entry> dataSet , String label){
        LineDataSet set = new LineDataSet(dataSet, label);
        return set;
    }

    private LineDataSet analysis(LineDataSet set , int lineColor){
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setCubicIntensity(0.2f);
        set.setDrawFilled(true);
        set.setDrawCircles(false);
        set.setLineWidth(3f);
        set.setCircleRadius(4f);
        set.setCircleColor(Color.YELLOW);
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setHighlightLineWidth(2f);
        set.setColor(lineColor);
        set.setFillColor(lineColor);
        set.setFillAlpha(60);
        set.setValueTextColor(Color.WHITE);
        set.setDrawHorizontalHighlightIndicator(false);
        set.setFillFormatter(new IFillFormatter() {
            @Override
            public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                return -10;
            }
        });

        return set;
    }


}
