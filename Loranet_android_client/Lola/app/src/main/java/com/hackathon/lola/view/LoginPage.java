package com.hackathon.lola.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hackathon.lola.MainActivity;
import com.hackathon.lola.R;

/**
 * Created by OlayBobby on 3/6/2017.
 */

public class LoginPage extends AppCompatActivity {
    private static Button admin_btn;
    private static Button user_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        adminOnClick();
        userOnClick();
    }

    public void adminOnClick(){
        admin_btn = (Button) findViewById(R.id.button_admin);
        admin_btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(LoginPage.this, "Admin Login Successfully!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        intent.setClass(LoginPage.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
        );
    }

    public void userOnClick(){
        user_btn = (Button) findViewById(R.id.button_user);

        user_btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(LoginPage.this, "User Login Successfully!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        intent.setClass(LoginPage.this,UserPage.class);
                        startActivity(intent);
                    }
                }
        );
    }

}
