package com.hackathon.lola.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.hackathon.lola.R;

/**
 * Created by ernestwong on 4/6/2017.
 */

public class StatusTextView extends TextView {
    public StatusTextView(Context context) {
        this(context , null);
    }

    public StatusTextView(Context context, AttributeSet attrs) {
        this(context, attrs , 0);
    }

    public StatusTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.text_status_font));
    }

    public void setTextStyle(Typeface typeface){
        setTypeface(typeface);
    }

    public void setStatus(int status){
        if(status == 1){
            setTextColor(Color.GREEN);
            setText("CONNECTED");
        }else if(status == 0){
            setTextColor(Color.RED);
            setText("DISCONNECT");
        }else if(status == 2){
            setTextColor(Color.GRAY);
            setText("PENDING");
        }
    }

}
