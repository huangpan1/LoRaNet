package com.hackathon.lola.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.hackathon.lola.R;

/**
 * Created by OlayBobby on 4/6/2017.
 */

public class UserPage extends AppCompatActivity {

    private static ImageButton weather_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_page);
        weatherOnClickListener();
    }

    public void weatherOnClickListener(){
        weather_btn = (ImageButton) findViewById(R.id.imageButton2);
        weather_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(UserPage.this, WeatherPage.class);
                        startActivity(intent);
                    }
                }
        );

    }


}
