package com.hackathon.lola.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hackathon.lola.R;

/**
 * Created by OlayBobby on 4/6/2017.
 */

public class WeatherPage extends AppCompatActivity {

    private static ImageButton think_btn;
    private static TextView myStatus;
    private static EditText wantToSay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_page);
        weatherOnClickListener();
    }

    public void weatherOnClickListener() {
        think_btn = (ImageButton) findViewById(R.id.imageButton7);
        myStatus = (TextView) findViewById(R.id.textView11);
        wantToSay = (EditText) findViewById(R.id.editText2);

        think_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String input = wantToSay.getText().toString();
                        myStatus.setText(input);
                    }
                }
        );
    }
}
