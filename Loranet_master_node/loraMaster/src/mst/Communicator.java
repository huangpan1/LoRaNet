package mst;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import org.json.simple.parser.JSONParser;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

public class Communicator implements Runnable, SerialPortEventListener {
	static CommPortIdentifier portId;
	static Enumeration portList;

	static InputStream inputStream;
	static OutputStream output;
	static SerialPort serialPort;
	static Thread readThread;

	int masterTimestamp = 1000;
	final static int ID = 11;
	static List<Integer> routeTable = new ArrayList<Integer>();
	List<Integer> dataTable = new ArrayList<Integer>();
	int parentID = 0;

	static List<String> ROMGList = new ArrayList<String>();

	static List<int[]> MSTMap = new ArrayList<>();

	public static void buildMST(int[][] map, int numberOfNode) {
		int size = numberOfNode;
		MST mst = new MST(size);

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (map[i][j] == -1) {
					map[i][j] = Integer.MAX_VALUE - 1;
				}
			}
		}
		mst.prim(map);
		int[] resultParent = new int[size];
		int[] resultDistance = new int[size];

		resultParent = mst.getMSTParent();
		resultDistance = mst.getMSTDistance();

		for (int i = 0; i < size; i++) {
			int[] tmpNode = new int[2];
			tmpNode[0] = i;
			tmpNode[1] = resultParent[i];
			main.MSTMap.add(i, tmpNode);
		}
	}

	public static void updateInfo(String nodeData) {
		try {

			URL url = new URL("http://34.211.132.145/send_data.php?" + nodeData);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			JSONParser parser = new JSONParser();
			while ((output = br.readLine()) != null) {

				System.out.println(output);
			}

			conn.disconnect();

		} catch (Exception e) {

		}
	}

	public static String refresh() {
		PacketManager packetManager = new PacketManager();
		int mapSize = main.ROMGList.size() + 1;
		int[][] map = new int[mapSize][mapSize];
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				if (i == j) {
					map[i][j] = 0;
				} else {
					map[i][j] = Integer.MAX_VALUE - 1;
				}
			}
		}

		for (int i = 0; i < main.ROMGList.size(); i++) {
			List<String> tmpNodeList = packetManager.getROMGIDList(main.ROMGList.get(i));
			List<String> tmpNeighbour = packetManager.getROMGNodeList(main.ROMGList.get(i));
			String tmpNode = tmpNodeList.get(0);

			for (int j = 0; j < tmpNeighbour.size(); j++) {
				String tmpNeighbourNode = tmpNeighbour.get(j);
				try {
					// System.out.println(i + " " + j);
					map[Integer.parseInt(tmpNode) - 11][Integer.parseInt(tmpNeighbourNode) - 11] = 1;
					map[Integer.parseInt(tmpNeighbourNode) - 11][Integer.parseInt(tmpNode) - 11] = 1;
				} catch (Exception e) {
				}

			}
		}

		buildMST(map, mapSize);
		String Msg = "MREF";
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		Msg += strDate;
		for (int i = 0; i < mapSize; i++) {

			Msg += Integer.toString(main.MSTMap.get(i)[0] + 11) + ":" + Integer.toString(main.MSTMap.get(i)[1] + 11)
					+ ":";
		}
		Msg += "=";
		return Msg;
	}

	public static void main(String[] args) {
		portList = CommPortIdentifier.getPortIdentifiers();

		while (portList.hasMoreElements()) {
			portId = (CommPortIdentifier) portList.nextElement();
			if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				System.out.print(portId.getName());
				if (portId.getName().equals("COM5")) {
					try {
						serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
					} catch (PortInUseException e) {
						System.out.println(e);
					}
					try {
						inputStream = serialPort.getInputStream();
					} catch (IOException e) {
						System.out.println(e);
					}

					serialPort.notifyOnDataAvailable(true);
					try {
						serialPort.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
								SerialPort.PARITY_NONE);
					} catch (UnsupportedCommOperationException e) {
						System.out.println(e);
					}

					try {
						// send new node request
						writeData("");
						// // listen for 10 sec
						SimpleRead();
						//
						// try {
						// Thread.sleep(10000);
						// } catch (InterruptedException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						// serialPort.removeEventListener();
						//
						// SimpleRead();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		}
	}

	public static void SimpleRead() throws IOException {

		try {
			serialPort.addEventListener(new Communicator());
			System.out.print("start read...");
		} catch (TooManyListenersException e) {
			System.out.println(e);
		}

		readThread = new Thread(new Communicator());
		readThread.start();

	}

	public static void writeData(String s) {
		try {
			serialPort.notifyOnDataAvailable(true);
			OutputStream mOutputToPort = serialPort.getOutputStream();
			System.out.println("beginning to Write . \r\n");
			byte[] data = s.getBytes();
			for (int i = 0; i < data.length; i++) {
				mOutputToPort.write(data[i]);
			}
			System.out.println("AT Command Written to Port. \r\n");
			mOutputToPort.flush();
		} catch (Exception ex) {
			System.out.println("error : " + ex);
		}
	}

	public void run() {

	}

	public void serialEvent(SerialPortEvent event) {
		switch (event.getEventType()) {
		case SerialPortEvent.BI:
		case SerialPortEvent.OE:
		case SerialPortEvent.FE:
		case SerialPortEvent.PE:
		case SerialPortEvent.CD:
		case SerialPortEvent.CTS:
		case SerialPortEvent.DSR:
		case SerialPortEvent.RI:
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
			break;
		case SerialPortEvent.DATA_AVAILABLE:

			String response = "";
			try {

				byte[] readBuffer = new byte[1];
				boolean complete = false;
				while (!complete) {
					int numBytes = inputStream.read(readBuffer);

					String tempresponse = (new String(readBuffer));
					// System.out.println("size: " +
					// tempresponse.trim().length());
					response += tempresponse.trim();

					// System.out.println("response: " + response);
					if (response.charAt(response.length() - 1) == '=') {
						complete = true;
						response = response.substring(0, response.length() - 1);
					}
				}
			} catch (IOException e) {
				System.out.println(e);
			}

			// response.trim();
			System.out.println(response + "\n");
			PacketManager packetManager = new PacketManager();

			// String packetMsg = "DAMG01123123456";

			String packetMsg = response;

			String packetHeader = packetManager.getHeader(packetMsg);
			if (packetHeader.equals("DAMG")) {
				int node = Integer.parseInt(packetManager.getDAMGNode(packetMsg));
				List nodeData = packetManager.getDAMGData(packetMsg);

				String getParam = "node_id=" + Integer.toString(node) + "&data=";
				getParam += "{\"value1\":\"" + nodeData.get(0) + "\",";
				getParam += "\"value2\":\"" + nodeData.get(1) + "\",";
				getParam += "\"value3\":\"" + nodeData.get(2) + "\"}";
				// System.out.print(getParam);
				updateInfo(getParam);
			}
			if (packetHeader.equals("ROMG")) {

				List<String> nodeList = packetManager.getROMGNodeList(packetMsg);
				List<String> idList = packetManager.getROMGIDList(packetMsg);

				boolean add = false;
				boolean update = false;
				for (int i = 0; i < main.ROMGList.size(); i++) {
					List<String> tmpList = packetManager.getROMGIDList(main.ROMGList.get(i));
					// System.out.print(tmpList);
					if (idList.equals(tmpList)) {
						main.ROMGList.set(i, packetMsg);
						update = true;
					}
				}
				if (update == false) {
					main.ROMGList.add(packetMsg);
					add = true;
				}

				// System.out.print(main.ROMGList);
				String Msg = refresh();
				writeData(Msg);
			}
			if (packetHeader.equals("NREQ")) {
				String Msg = "NRES" + packetManager.getNREQID(packetMsg)+ ID + "=";
				writeData(Msg);
			}

			if (response.contains("NRES")) {
				System.out.println("Contain NRES: " + response.substring(4, 6));
				routeTable.add(Integer.valueOf(response.substring(4, 6)));
				String msg = "ROMG" + ID + "__";
				for (int i : routeTable) {
					msg += ID;
					msg += i;
				}
				System.out.println("msg : " + msg);
				writeData(msg);
			}

			// else if (response.contains("MREF")) {
			// System.out.println("Contain MREF: " + response.substring(4, 18));
			// long times = Long.valueOf(response.substring(4, 18));
			// System.out.println("test: " + times);
			// if (times != masterTimestamp) {
			// // masterTimestamp = times;
			// System.out.println("test1: " + response.substring(18));
			// String[] s = response.substring(18).split(":");
			// for (int i = 0; i < s.length; i++) {
			// System.out.println(" test ++ " + s[i]);
			// if (Integer.valueOf(s[i]) == ID) {
			// parentID = Integer.valueOf(s[i + 1]);
			// writeData(response);
			// System.out.println("parentID: " + parentID);
			// i++;
			// } else {
			// i++;
			// }
			// }
			// }
			//
			// }

			break;
		}
	}
}