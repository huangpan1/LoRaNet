/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mst;

/**
 *
 * @author kwongpinken
 */
public class MST {

    int numberOfNode;
    int mstparent[];
    int mstDistance[];
    
    
    MST(int size){
        this.numberOfNode = size;
        this.mstDistance = new int[this.numberOfNode];
        this.mstparent = new int[this.numberOfNode];
    }

    public boolean updateNumberOfNode(int numberOfNode) {
        this.numberOfNode = numberOfNode;
        return true;
    }

    public void prim(int map[][]) {
        int adj[][] = map;
        int d[] = new int[this.numberOfNode];       // 記錄目前的MST到圖上各點的距離
        int parent[] = new int[this.numberOfNode];  // 記錄各個點在MST上的父親是誰
        boolean visit[] = new boolean[this.numberOfNode];   // 記錄各個點是不是已在MST之中

        for (int i = 0; i < this.numberOfNode; i++) {
            visit[i] = false;
        }
        for (int i = 0; i < this.numberOfNode; i++) {
            d[i] = Integer.MAX_VALUE;
        }
        d[0] = 0;
        parent[0] = 0;

        for (int i = 0; i < this.numberOfNode; i++) {
            int a = -1, b = -1, min = Integer.MAX_VALUE;
            for (int j = 0; j < this.numberOfNode; j++) {
                if (!visit[j] && d[j] < min) {
                    a = j;  
                    min = d[j];
                }
            }
            if (a == -1) {
                break;
            }
            visit[a] = true;
            
            for (b = 0; b < this.numberOfNode; b++) {
                if (!visit[b] && adj[a][b] < d[b]) {
                    d[b] = adj[a][b];
                    parent[b] = a;
                }
            }         
        }
        this.mstparent = parent;
        this.mstDistance = d;
    }
    
    public int[] getMSTParent(){
        return this.mstparent;
    }
    
    public int[]  getMSTDistance(){
        return this.mstDistance;
    }
}
