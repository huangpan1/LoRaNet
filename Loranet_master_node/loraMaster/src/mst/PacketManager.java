package mst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kwongpinken
 */
public class PacketManager {

    public String getHeader(String packet) {
        String header = packet.substring(0, 4);
        return header;
    }

    public String getDAMGNode(String packet) {
        return packet.substring(4, 6);
    }

    public List getDAMGData(String packet) {
        List<String> value = new ArrayList<String>();
        for (int i = 8; i < packet.length(); i += 3) {
            value.add(packet.substring(i, i + 3));
        }
        return value;
    }

    public List getROMGNodeList(String packet) {
        List<String> nodeList = new ArrayList<String>();
        for (int i = 8; i < packet.length(); i = i + 4) {
            String node = packet.substring(i, i + 2);
            String distance = packet.substring(i+2, i+4);
           
            if(!node.equals("00") && !distance.equals("00")){
//                System.out.println("node : " + node + " distance :" + distance);
                nodeList.add(node);
            }
            
        }

        return nodeList;
    }

    public List getROMGIDList(String packet) {
        String FromID = "";
        String ToID = "";
        
        List<String> idList = new ArrayList<>();
        
        FromID = packet.substring(4, 6);
        ToID = packet.substring(8, 10);
        
        idList.add(0, FromID);
        idList.add(1, ToID);
    
        return idList;
    }

    public String getNREQID(String packet) {
        String id = packet.substring(4, 6);
        return id;
    }

    public String getNRESID(String packet) {
        String id = packet.substring(4, 6);
        return id;
    }

    public String getMREFTimestamp(String packet) {
        return packet.substring(4, 18);
    }

    public List getMREFNodeList(String packet) {
        List<String> node = new ArrayList<String>();
        List<String> parent = new ArrayList<String>();
        List<List> nodeList = new ArrayList<>();

        for (int i = 16; i < packet.length(); i += 4) {
            String tmpNode = packet.substring(i, i + 2);
            String tmpParent = packet.substring(i + 3, i + 4);
            node.add(tmpNode);
            parent.add(tmpParent);
        }

        nodeList.add(0, node);
        nodeList.add(1, parent);
        return nodeList;
    }

    public String BuildMREF(List nodeList) {
        String MREF = "";
        return MREF;
    }
}
