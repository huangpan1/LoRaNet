/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mst;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author kwongpinken
 */
public class main {

    static List<String> ROMGList = new ArrayList<String>();

    static List< int[]> MSTMap = new ArrayList<>();

    public static void buildMST(int[][] map, int numberOfNode) {
        int size = numberOfNode;
        MST mst = new MST(size);

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (map[i][j] == -1) {
                    map[i][j] = Integer.MAX_VALUE - 1;
                }
            }
        }
        mst.prim(map);
        int[] resultParent = new int[size];
        int[] resultDistance = new int[size];

        resultParent = mst.getMSTParent();
        resultDistance = mst.getMSTDistance();

        for (int i = 0; i < size; i++) {
            int[] tmpNode = new int[2];
            tmpNode[0] = i;
            tmpNode[1] = resultParent[i];
            main.MSTMap.add(i, tmpNode);
        }
    }

    public static void updateInfo(String nodeData) {
        try {

            URL url = new URL("http://34.211.132.145/send_data.php?" + nodeData);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            JSONParser parser = new JSONParser();
            while ((output = br.readLine()) != null) {

                System.out.println(output);
            }

            conn.disconnect();

        } catch (Exception e) {

        }
    }

    public static String refresh() {
        PacketManager packetManager = new PacketManager();
        int mapSize = main.ROMGList.size() + 1;
        int[][] map = new int[mapSize][mapSize];
        for (int i = 0; i < mapSize; i++) {
            for (int j = 0; j < mapSize; j++) {
                if (i == j) {
                    map[i][j] = 0;
                } else {
                    map[i][j] = Integer.MAX_VALUE - 1;
                }
            }
        }
//        System.out.println("loop");
        for (int i = 0; i < main.ROMGList.size(); i++) {
            List<String> tmpNodeList = packetManager.getROMGIDList(main.ROMGList.get(i));
            List<String> tmpNeighbour = packetManager.getROMGNodeList(main.ROMGList.get(i));
            String tmpNode = tmpNodeList.get(0);

            for (int j = 0; j < tmpNeighbour.size(); j++) {
                String tmpNeighbourNode = tmpNeighbour.get(j);
                try {
                    System.out.println(i + " " + j);
                    map[Integer.parseInt(tmpNode) - 11][Integer.parseInt(tmpNeighbourNode) - 11] = 1;
                    map[Integer.parseInt(tmpNeighbourNode) - 11][Integer.parseInt(tmpNode) - 11] = 1;
                } catch (Exception e) {
                }

            }
        }

        buildMST(map, mapSize);
        String Msg = "MREF";
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        Msg += strDate;
        for (int i = 0; i < mapSize; i++) {
        	int tmpChildren = main.MSTMap.get(i)[0] + 11;
        	int tmpParent = main.MSTMap.get(i)[1] + 11;
        	System.out.print(tmpChildren);
            Msg += Integer.toString(tmpChildren) + ":" + Integer.toString(tmpParent) + ":";
        }
        return Msg;
    }

    public static String serialRead() {
        String msg = "";
        return msg;
    }

    /**
     * @param args the command line arguments
     */
    public static void run(String[] args) {
//      public static void run(){      

        PacketManager packetManager = new PacketManager();
        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {

//            String packetMsg = "DAMG01123123456";
            String packetMsg = input.nextLine();

            String packetHeader = packetManager.getHeader(packetMsg);
            if (packetHeader.equals("DAMG")) {
                int node = Integer.parseInt(packetManager.getDAMGNode(packetMsg));
                List nodeData = packetManager.getDAMGData(packetMsg);

                String getParam = "node_id=" + Integer.toString(node) + "&data=";
                getParam += "{\"value1\":\"" + nodeData.get(0) + "\",";
                getParam += "\"value2\":\"" + nodeData.get(0) + "\",";
                getParam += "\"value3\":\"" + nodeData.get(0) + "\"}";
                System.out.print(getParam);
                updateInfo(getParam);
            }
            if (packetHeader.equals("ROMG")) {

                List<String> nodeList = packetManager.getROMGNodeList(packetMsg);
                List<String> idList = packetManager.getROMGIDList(packetMsg);

                boolean add = false;
                boolean update = false;
                for (int i = 0; i < main.ROMGList.size(); i++) {
                    List<String> tmpList = packetManager.getROMGIDList(main.ROMGList.get(i));
                    System.out.print(tmpList);
                    if (idList.equals(tmpList)) {
                        main.ROMGList.set(i, packetMsg);
                        update = true;
                    }
                }
                if (update == false) {
                    main.ROMGList.add(packetMsg);
                    add = true;
                }

                System.out.print(main.ROMGList);
                String Msg = refresh();
                System.out.print(Msg);
            }
        }

//        PacketManager packetManager = new PacketManager();
//        while (true) {
//
//        }  
//        int mapSize = 7;
//        int[][] map = {
//            {0, 1, 1, -1, -1, -1, -1},
//            {1, 0, -1, 1, -1, -1, -1},
//            {1, -1, 0, 1, 1, -1, -1},
//            {-1, 1, 1, 0, -1, 1, -1},
//            {-1, -1, 1, -1, 0, 1, 1},
//            {-1, -1, -1, 1, 1, 0, -1},
//            {-1, -1, -1, -1, 1, -1, 0}
//        };
    }
}
