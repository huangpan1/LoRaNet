import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.Timestamp;
import java.util.*;
import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;

public class Communicator implements Runnable, SerialPortEventListener, Serializable {
    static CommPortIdentifier portId;
    static Enumeration portList;

    static InputStream inputStream;
    static OutputStream output;
    static SerialPort serialPort;
    static Thread readThread;
    
    int masterTimestamp = 1000;
	final static int ID = 12;
	static List<Integer> routeTable = new ArrayList<Integer>();
	List<Integer> dataTable = new ArrayList<Integer>();
	static int parentID = 0;

    public static void main(String[] args) {
    	
    	
    	
        portList = CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                 if (portId.getName().equals("COM5")) {
                	 
                	 try {
                         serialPort = (SerialPort) portId.open("SimpleReadApp", 2000);
                     } catch (PortInUseException e) {System.out.println(e);}
                     try {
                         inputStream = serialPort.getInputStream();
                     } catch (IOException e) {System.out.println(e);}
             	
                     serialPort.notifyOnDataAvailable(true);
                     try {
                         serialPort.setSerialPortParams(9600,
                             SerialPort.DATABITS_8,
                             SerialPort.STOPBITS_1,
                             SerialPort.PARITY_NONE);         
                     } catch (UnsupportedCommOperationException e) {System.out.println(e);} 
                     
                	 			try {
                	 				//send new node request
                    	 			writeData("");
                	 				//listen for 10 sec
									SimpleRead();
									
									
									
									
									/*
										try {
											Thread.sleep(10000);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										serialPort.removeEventListener();
									
									
									if(!routeTable.isEmpty()){
										SimpleRead();
									}*/
									
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                	 			
                }
            }
        }
/*
		while(true){
        
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("loop");
			        	if(parentID != 0){
			        		writeData("DANG" + ID + parentID + "255256267");
			        }
		}
*/
        }
        
        
        	
        	
        
    

    public static void SimpleRead() throws IOException {
       
    	try {
            serialPort.addEventListener(new Communicator());
	} catch (TooManyListenersException e) {System.out.println(e);}	

        readThread = new Thread(new Communicator());
        readThread.start();
        
        
        
        
        
    }
    
    
    
    public static void writeData(String s)
    {
    	try{
    		serialPort.notifyOnDataAvailable(true);
            OutputStream mOutputToPort = serialPort.getOutputStream();
            if(s.isEmpty()){
            s = "NREQ" + String.valueOf(ID) + "=";
            }
            System.out.println("beginning to Write . \r\n");
            byte[] data = s.getBytes();
            for(int i=0;i<data.length;i++){
            mOutputToPort.write(data[i]);}
            System.out.println(s+" \r\n");
            mOutputToPort.flush();
    	}catch(Exception ex){
    		System.out.println("error : " + ex);
    	}
    }

    public void run() {
 
    }

    public void serialEvent(SerialPortEvent event) {
        switch(event.getEventType()) {
        case SerialPortEvent.BI:
        case SerialPortEvent.OE:
        case SerialPortEvent.FE:
        case SerialPortEvent.PE:
        case SerialPortEvent.CD:
        case SerialPortEvent.CTS:
        case SerialPortEvent.DSR:
        case SerialPortEvent.RI:
        case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
            break;
        case SerialPortEvent.DATA_AVAILABLE:
            
        	String response ="";
            try {

            	byte[] readBuffer = new byte[1];
            	boolean complete = false;
            	while(!complete){
                    int numBytes = inputStream.read(readBuffer);
                    
                    String tempresponse = (new String(readBuffer));
                    //System.out.println("size: " + tempresponse.trim().length());
                    response += tempresponse.trim();
                    if(response.charAt(response.length()-1) == '='){
                    	complete = true;
                    }
                    
                }  
                } catch (IOException e) {System.out.println(e);}
                
                	//response.trim();
                    System.out.println(response + "\n");
                    if(response.contains("NRES")){
                    	System.out.println("Contain NRES: " + response.substring(4,6));
                    	if(Integer.valueOf(response.substring(4,6)) == ID){
                    		routeTable.add(Integer.valueOf(response.substring(6,8)));
                    	String msg = "ROMG" + ID + "__";
                    	for(int i : routeTable){
                    		msg += i;
                    		msg += "01";
                    		
                    	}
                    	System.out.println("msg : " + msg);
                    	writeData(msg+"=");
                    	}
                    	
                    	
                    }else if(response.contains("MREF")){
                    	System.out.println("Contain MREF: " + response.substring(4, 18));
                    	long times = Long.valueOf(response.substring(4, 18));
                    	System.out.println("test: " + times);
                    	if(times != masterTimestamp){
                    		//masterTimestamp = times;
                    		System.out.println("test1: " + response.substring(18));
                    		String[] s = response.substring(18).split(":");
                    		for(int i=0; i<s.length; i++){
                    			System.out.println(" test ++ "+ s[i]);
                    			if(Integer.valueOf(s[i]) == ID){
                    				parentID = Integer.valueOf(s[i+1]);
                    				writeData(response+"=");
                    				System.out.println("parentID: " + parentID);
                    				i++;
                    			}
                    			else{
                    				i++;
                    			}
                    		}
                    	}
                    	   
            }else if(response.contains("ROMG")){
            	if(parentID != 0 && (response.substring(6,8).equals(String.valueOf(ID)) || response.substring(6,8).equals("__"))){
            		String msg = response.substring(0,6);
            		msg+=parentID;
            		msg+=response.substring(8);
            		System.out.println("ROMG msg: " + msg);
            		writeData(msg+"=");
            		
            	}
            }else if(response.contains("DAMG")){
            	if(response.substring(6, 8).equals(String.valueOf(ID))){
            		String msg = response.substring(0,6);
            		msg+=parentID;
            		msg+=response.substring(8);
            		System.out.println("DAMG msg: " + msg);
            		writeData(msg+"=");
            	}
            }
           
            break;
        }
    }
}