<?php

require_once("./model/class.dataManager.php");

$data_manager = new DataManager();
$data = $data_manager->get_table_data();

$result = array(
    "status" => 200,
    "data" => $data
);
$result = json_encode($result);
echo $result;