<?php

require_once("./model/class.dataManager.php");

$node_id = isset($_GET['node_id']) ? $_GET['node_id'] :  null;

if($node_id != null){
    $data_manager = new DataManager();
    $data = $data_manager->get_data_details($node_id);
  
    $result = array(
        "status" => 200,
        "data" => $data
    );
}else{
    $result = array(
        "status" => 500,
        "data" => null
    );
}

$result = json_encode($result);
echo $result;