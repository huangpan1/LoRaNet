<?php
error_reporting(E_ALL);

require_once("class.configManager.php");

class DataManager{

    function get_table_data(){
        $node_id_list = $this->get_node_id_list();
        $data = array();
        for($i = 0;$i< count($node_id_list); $i ++){
          $data[$i] = $this->get_data($node_id_list[$i]['node_id']);
        }
        return $data;
    }

    function get_data($node_id){
        $config_manager = new ConfigManager();
        $conn = $config_manager->db_connect();

        $sql = "SELECT data.data_id,data.node_id, data.data, data.timestamp, node.node_id, node.node_name
        from data, node
        where data.node_id = node.node_id and data.node_id = ?
        ORDER BY data.timestamp DESC limit 1";

        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $node_id);
        $stmt->execute();
        $result = $stmt->get_result();

        $query_data = array();

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $query_data[] = $row;
        }

        $status = 1;
        $data = array();
        date_default_timezone_set('Asia/Hong_Kong');
        $date = date('Y-m-d H:i:s', time() - 20);
        $querydate = date('Y-m-d H:i:s', $query_data[$i]['timestamp']);
     
        if($querydate > $date){
            $status = 1;
        }

        for($i = 0; $i < count($query_data);$i++){
            $tmp_data = json_decode($query_data[$i]['data'],true);

            $key = array_keys($tmp_data);
            for($j = 0;$j < count($key);$j++){
                $tmp_data[$key[$j]] = (int)$tmp_data[$key[$j]];
            }
            $data[$i] = array(
                "status" => $status,
                "data_id"=> $query_data[$i]['data_id'],
                "node_id"=> $query_data[$i]['node_id'],
                "node_name"=> $query_data[$i]['node_name'],
                "timestamp"=> $query_data[$i]['timestamp'],
                "data" => $tmp_data,
            );
        }
        return $data[0];
    }

    function get_data_details($node_id){
        $config_manager = new ConfigManager();
        $conn = $config_manager->db_connect();

        $sql = "SELECT data.data_id,data.node_id, data.data, data.timestamp, node.node_id, node.node_name
        from data, node
        where data.node_id = node.node_id and data.node_id = ?
        ORDER BY data.timestamp DESC";

        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $node_id);
        $stmt->execute();
        $result = $stmt->get_result();

        $query_data = array();

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $query_data[] = $row;
        }

        $data = array(
            "timestamp"=>array()
        );

        for($i = 0; $i < count($query_data);$i++){
            $tmp_data = json_decode($query_data[$i]['data'],true);
            // print_r($tmp_data);
            $data['timestamp'][$i] = $query_data[$i]['timestamp'];
            $key = array_keys($tmp_data);
            for($j = 0;$j < count($key);$j++){
                $data[$key[$j]][$i] = (int)$tmp_data[$key[$j]];
            }
        }
        return $data;
    }

    function get_node_id_list(){
        $config_manager = new ConfigManager();
        $conn = $config_manager->db_connect();
        $sql = "SELECT node.node_id, node.node_name FROM node";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->get_result();

        $query_data = array();

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $query_data[] = $row;
        }
        return $query_data;
    }

    function insert_data($node_id, $data){
        date_default_timezone_set('Asia/Hong_Kong');
        $date = date('Y-m-d H:i:s', time());
        $config_manager = new ConfigManager();
        $conn = $config_manager->db_connect();
        $sql = "INSERT INTO `data`(`node_id`, `timestamp`, `data`) VALUES (?,?,?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("iss",$node_id,$date,$data);
        $stmt->execute();
        $result = $stmt->get_result();
        return $this->get_table_data();
    }
}