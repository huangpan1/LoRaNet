<?php

require_once("./model/class.dataManager.php");

$node_id = isset($_GET['node_id']) ? $_GET['node_id'] :  null;
$data = isset($_GET['data']) ? $_GET['data'] :  null;

if($node_id != null && $data != null){
    $data_manager = new DataManager();
    $data = $data_manager->insert_data($node_id,$data);
  
    $result = array(
        "status" => 200,
        "data" => $data
    );
}else{
    $result = array(
        "status" => 500,
        "data" => null
    );
}

$result = json_encode($result);
echo $result;